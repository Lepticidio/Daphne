﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampTrigger : MonoBehaviour {

	public Lamp lamp;
	public Gaia gaia;

	// Use this for initialization
	void Awake () {

		gaia = GameObject.Find ("Gaia").GetComponent<Gaia> ();

	}

	void OnTriggerEnter2D(Collider2D col){

		if (gaia.lampAggressive) {
			if (col.tag == "Player" && !lamp.falling) {
				lamp.Fall ();
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour {

	public Character Eris, Gaia, Tant;
	public List<List<Sentence>> dialogues = new List<List<Sentence>>();


	// Use this for initialization
	void Awake () {

		Eris = new Character ("Eris");
		Eris.color = new Color (0.5961f, 0.2706f, 0.5961f);

		Gaia = new Character ("Gaia");
		Gaia.color = new Color (0.2706f, 0.2706f, 0.5961f);

		Tant = new Character ("Tant");
		Tant.color = new Color (0.5961f, 0.2706f, 0.2706f);

		List<Sentence> firstDeath = new List<Sentence> ();
		dialogues.Add (firstDeath);

		firstDeath.Add (new Sentence (Eris,
			"Nope!",
			"¡Nope!"
		));
		firstDeath.Add(new Sentence(Tant,
			"WHAT THE FUCK! Did I just die?!",
			"¡QUÉ COÑO! ¡¿Acabo de morir?!"
		));
		firstDeath.Add (new Sentence (Eris,
			"Lets say I just stopped time before that happened.",
			"Digamos que he parado el tiempo antes de que eso pasara."
		));
		firstDeath.Add (new Sentence (Eris,
			"I've saved your life. You should be thankful.",
			"He salvado tu vida. Deberías estar agradecido."
		));
		firstDeath.Add(new Sentence(Tant,
			"I think I need a minute to recover from almost DYING.",
			"Creo que necesito un momento para recuperarme de casi MORIR."
		));
		firstDeath.Add (new Sentence (Eris,
			"What a chicken you are.",
			"Eres un gallina."
		));
		firstDeath.Add(new Sentence( Gaia,
			"I don't know who you are, but please, don't alter the world's physics.",
			"No sé quién eres, pero por favor, no alteres las físicas del mundo."
		));
		firstDeath.Add (new Sentence (Eris,
			"Look who's talking! Lockers don't fall on their own, you know.",
			"¡Mira quién fue a hablar! Las taquillas no suelen caerse solas."
		));
		firstDeath.Add(new Sentence( Gaia,
			"I'll try to modify physics as little as possible.",
			"Intentaré modificar las físicas lo menos posible."
		));
		firstDeath.Add(new Sentence( Gaia,
			"In spite of that, I can't allow you to continue. I'm sorry.",
			"A pesar de eso, no puedo permitirte continuar. Lo siento."
		));
		firstDeath.Add (new Sentence (Eris,
			"Ignore her, you promised to continue no matter what.",
			"Ignórala, prometiste continuar pasara lo que pasara."
		));


		List<Sentence> secondDeath = new List<Sentence> ();
		dialogues.Add (secondDeath);
		secondDeath.Add(new Sentence( Gaia,
			"Please, stop trying to speak to Daphne.",
			"Por favor, deja de intentar hablar con Daphne."
		));
		secondDeath.Add (new Sentence (Eris,
			"Don't mind her. Lets try again.",
			"No le hagas caso. Volvamos a intentarlo."
		));

	}


}
	






﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeController : MonoBehaviour {

	public bool fading;
	public bool increase;
	public float fadeSpeed;
	public Image image;

	void FixedUpdate(){

		if (increase && fading && image.color.a < 1) {
			image.color = new Color (image.color.r, image.color.g, image.color.b, image.color.a + fadeSpeed);
		} else if (!increase && fading&&image.color.a > 0) {
			image.color = new Color (image.color.r, image.color.g, image.color.b, image.color.a - fadeSpeed);
		}

	}

}

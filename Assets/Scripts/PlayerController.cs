﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public CharacterMovement CM;

	
	// Update is called once per frame
	void FixedUpdate () {


		if (Input.GetKey (KeyCode.D )||Input.GetKey( KeyCode.RightArrow)) {
			if (!Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.RightShift)) {
				CM.WalkRight ();
			} else {
				CM.RunRight ();
			}
		}
		if (Input.GetKey (KeyCode.A )||Input.GetKey( KeyCode.LeftArrow)) {
			if (!Input.GetKey (KeyCode.LeftShift) && !Input.GetKey (KeyCode.RightShift)) {
				CM.WalkLeft ();
			} else {
				CM.RunLeft ();
			}
		}
		if(!(Input.GetKey (KeyCode.D )||Input.GetKey( KeyCode.RightArrow))&&!(Input.GetKey (KeyCode.A )||Input.GetKey( KeyCode.LeftArrow))){
			CM.Idle ();
		}
	}
}

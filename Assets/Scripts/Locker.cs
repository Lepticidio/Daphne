﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Locker : MonoBehaviour {

	public bool falling, fallen;
	public Animator ani;
	public BoxCollider2D boxCollider, deathCollider;
	public Gaia gaia;

	// Use this for initialization
	void Awake () {

		gaia = GameObject.Find ("Gaia").GetComponent<Gaia> ();
		ani.speed = 0;

	}

	void OnTriggerEnter2D(Collider2D col){

		if (gaia.lockerAggressive) {
			if (col.tag == "Player" && !falling) {
				Fall ();
			}
		}
	}


	void Fall(){
		ani.speed = 1;
		falling = true;
		Invoke ("ReadyToKill", 0.5f);
		Invoke ("Stop", 1);
	}
	void Stop(){
		falling = false;
		fallen = true;
		boxCollider.enabled = true;
		gameObject.tag = "Floor";
	}
	void ReadyToKill(){
		deathCollider.enabled = true;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour {

	public bool falling, fallen;
	public Sprite off;
	public SpriteRenderer sr;
	public Gaia gaia;

	// Use this for initialization
	void Awake () {

		gaia = GameObject.Find ("Gaia").GetComponent<Gaia> ();

	}
	void OnCollisionEnter2D(Collision2D col){

		if (col.gameObject.tag == "Floor"&&falling&&!fallen) {
			
			Stop ();
		}

	}

	// Update is called once per frame
	public void Fall () {
		falling = true;
		gameObject.tag = "Deadly";
		gameObject.AddComponent<Rigidbody2D> ();
		sr.sprite = off;
	}
	// Update is called once per frame
	public void Stop() {
		falling = false;
		fallen = true;
		gameObject.tag = "Floor";
	}

}

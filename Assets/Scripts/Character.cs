﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character  {

	bool discovered;
	public string name;
	public Color color;
	public List<List<string[]>> dialogues = new List<List<string[]>> ();

	public Character (string nam){
		name = nam;
	}

}

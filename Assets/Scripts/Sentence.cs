﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sentence  {

	public Character character;
	public string[] sentence = new string[2];

	public Sentence (Character charac, string eng, string esp){
		character = charac;
		sentence [0] = eng;
		sentence [1] = esp;
	}


}

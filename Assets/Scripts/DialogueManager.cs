﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueManager : MonoBehaviour {

	public bool dialogueTime, sending;
	public int currentSentence;
	public float pauseTime;
	public Gaia gaia;
	public TextProcessor textProcessor;
	public Data data;

	void FixedUpdate   (){
		if (dialogueTime && !textProcessor.writing&&!sending) {
			sending = true;
			Invoke ("SendText", pauseTime);
		}
		if (dialogueTime&&Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (KeyCode.KeypadEnter)) {
			SendText ();
		}
	}

	void SendText(){
		List<Sentence> dialogue = data.dialogues [gaia.nextDialogue];
		Sentence sentence = dialogue[currentSentence];
		textProcessor.DialogueBox.color = sentence.character.color;
		textProcessor.WriteText (sentence.sentence [gaia.language]);
		if (currentSentence < dialogue.Count - 1) {
			currentSentence++;
		} else {
			Invoke("EndDialogue",0.2f);
		}
	}
	void EndDialogue(){
		dialogueTime = false;
		currentSentence = 0;
	}

}

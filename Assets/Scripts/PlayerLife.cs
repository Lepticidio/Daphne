﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLife : MonoBehaviour {

	public bool dead;
	public FadeController fade;
	public PlayerController playerController;
	public DialogueManager dialogueManager;

	void Awake(){
		fade = gameObject.GetComponent<FadeController> ();
		playerController = gameObject.GetComponent<PlayerController> ();
		dialogueManager = GameObject.Find ("Gaia").GetComponent<DialogueManager> ();


	}

	void Start(){
		fade.fading = true;
		Invoke ("Birth", 2);
	}
	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Deadly") {
			Death ();
		}
	}
	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Deadly") {
			Death ();
		}
	}
	public void Birth(){
		fade.fading = false;
		playerController.enabled = true;
		fade.increase = true;
	}
	public void Death(){

		if (!dead) {
			fade.fading = true;
			playerController.enabled = false;
			playerController.CM.Idle ();

			Rigidbody2D[] rbs = FindObjectsOfType <Rigidbody2D> ();
			for (int i = 0; i < rbs.Length; i++) {
				Destroy(rbs[i]);
			}
			Animator[] anims = FindObjectsOfType<Animator> ();
			for (int i = 0; i < anims.Length; i++) {
				if (anims [i] != playerController.CM.ani) {
					anims [i].speed = 0;
				} 
			}
			dialogueManager.dialogueTime = true;
			dead = true;
		}
	}
}

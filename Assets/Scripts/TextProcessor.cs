﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextProcessor: MonoBehaviour {

	public bool writing;
	public int CurrentChar;
	public float CharacterTime;
	public string CurrentText ="", wholeText ="";
	public Text DialogueBox;

	public void WriteText(string text){
		Clear ();
		wholeText = text;
		writing = true;
		WriteNext ();
	}

	void WriteNext(){
		if (writing) {
			if (CurrentChar < wholeText.Length) {
				CurrentText += wholeText [CurrentChar];
				CurrentChar++;
				DialogueBox.text = CurrentText;
				Invoke ("WriteNext", CharacterTime);
			} else {
				writing = false;
			}

			
		}
	}
	public void Clear(){
		CurrentChar = 0;
		CurrentText = "";
		wholeText = "";
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour {


	bool right= true, idle = true, walking, running;
	public float speed, runningSpeed;
	public Animator ani;
	// Use this for initialization
	void Awake () {
		ani = gameObject.GetComponent<Animator> ();
	}
	public void Idle(){
		if (!idle) {
			ani.SetTrigger ("StartIdle");
			walking = false;
			running = false;
			idle = true;
		}
	}
	public void WalkRight(){
		transform.position = transform.position + new Vector3 (speed, 0, 0);
		if (!right) {
			Turn ();
			right = true;
		}
		if (!walking) {
			ani.SetTrigger ("StartWalking");
			idle = false;
			walking = true;
			running = false;
		}
	}
	public void WalkLeft(){
		transform.position = transform.position + new Vector3 (-speed, 0, 0);
		if (right) {
			Turn ();
			right = false;
		}
		if (!walking) {
			ani.SetTrigger ("StartWalking");
			idle = false;
			walking = true;
			running = false;
		}
	}

	public void RunRight(){
		transform.position = transform.position + new Vector3 (runningSpeed, 0, 0);
		if (!right) {
			Turn ();
			right = true;
		}
		if (!running) {
			ani.SetTrigger ("StartRunning");
			idle = false;
			walking = false;
			running = true;
		}
	}
	public void RunLeft(){
		transform.position = transform.position + new Vector3 (-runningSpeed, 0, 0);
		if (right) {
			Turn ();
			right = false;
		}
		if (!running) {
			ani.SetTrigger ("StartRunning");
			idle = false;
			walking = false;
			running = true;
		}
	}
	void Turn(){
		transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
	}
}

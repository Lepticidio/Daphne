﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gaia : MonoBehaviour {

	public bool lampAggressive, lockerAggressive, deadOnce;
	public int nextDialogue, language;
	public PlayerLife playerLife;
	public DialogueManager dialogueManager;

	void Awake(){
		dialogueManager = gameObject.GetComponent<DialogueManager> ();
		LoadPast ();
		if (deadOnce) {
			nextDialogue = 1;
		}
	}

	// Update is called once per frame
	void FixedUpdate () {



		if (playerLife.dead&&!dialogueManager.dialogueTime) {
			if (Input.GetKey (KeyCode.Space)) {				
				Rewind ();
			}
		}
		if (Input.GetKey (KeyCode.Delete)) {
			Reset ();
		}

	}


	void Rewind(){
		if (!deadOnce) {
			PlayerPrefs.SetInt ("deadOnce", 1);
		}
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}
	void LoadPast(){
		if (PlayerPrefs.GetInt ("deadOnce") == 1) {
			deadOnce = true;
		}
	}
	void Reset(){
		PlayerPrefs.SetInt ("deadOnce", 0);
		deadOnce = false;
		nextDialogue = 0;
	}
}
